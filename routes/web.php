<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'index', 'uses' => 'FrontEndController@index']);
Route::get('/dashboard', ['as' => 'dashboard', 'uses' => 'AdminController@dashboard']);
Route::get('/sectors', ['as' => 'sectors', 'uses' => 'AdminController@sectors']);
Route::get('/markets', ['as' => 'markets', 'uses' => 'AdminController@markets']);
Route::get('/shops', ['as' => 'shops', 'uses' => 'AdminController@shops']);
Route::get('/categories', ['as' => 'categories', 'uses' => 'AdminController@categories']);
Route::get('/products', ['as' => 'products', 'uses' => 'AdminController@products']);
Route::get('/pubs', ['as' => 'pubs', 'uses' => 'AdminController@pubs']);
Route::get('/devenirApacheur', ['as' => 'devenirApacheur', 'uses' => 'FrontEndController@devenirApacheur']);
Route::post('/createUser', ['as' => 'createUser', 'uses' => 'AdminController@createUser']);
Route::get('/login', ['as' => 'login', 'uses' => 'AdminController@login']);
Route::post('/log', ['as' => 'log', 'uses' => 'AdminController@log']);
Route::post('/createSector', ['as' => 'createSector', 'uses' => 'AdminController@createSector']);
Route::post('/createMarket', ['as' => 'createMarket', 'uses' => 'AdminController@createMarket']);
Route::post('/createShop', ['as' => 'createShop', 'uses' => 'AdminController@createShop']);
Route::post('/createCategory', ['as' => 'createCategory', 'uses' => 'AdminController@createCategory']);
Route::post('/createProduct', ['as' => 'createProduct', 'uses' => 'AdminController@createProduct']);
Route::get('/productDetail{id}', ['as' => 'productDetail', 'uses' => 'FrontEndController@detailProduct']);
Route::get('/shops', ['as' => 'shops', 'uses' => 'FrontEndController@shops']);
Route::get('/shopDetail{id}', ['as' => 'shopDetail', 'uses' => 'FrontEndController@shopDetail']);
Route::get('/markets', ['as' => 'markets', 'uses' => 'FrontEndController@markets']);
Route::get('/marketDetail{id}', ['as' => 'marketDetail', 'uses' => 'FrontEndController@marketDetail']);
