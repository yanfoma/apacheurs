<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Market extends Model 
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'markets';

    protected $fillable = [
        'name', 'description', 'sector_id','adresse','created_by',
    ];

}
