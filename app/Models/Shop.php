<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model 
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shops';

    protected $fillable = [
        'name', 'description', 'market_id','phone', 'email', 'manager','created_by',
    ];

}
