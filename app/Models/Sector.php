<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model 
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sectors';

    protected $fillable = [
        'name', 'description', 'city','country','created_by',
    ];

}
