<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model 
{
   
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products_categories';

    protected $fillable = [
        'name', 'description','created_by',
    ];
}
