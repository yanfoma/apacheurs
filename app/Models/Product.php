<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model 
{
   
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    protected $fillable = [
        'name', 'description', 'product_category_id','shop_id', 'price', 'quantity', 'image','created_by',
    ];

}
