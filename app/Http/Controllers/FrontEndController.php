<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Market;
use App\Models\Shop;
use App\Models\Product;
use App\Models\Category;

class FrontEndController extends Controller
{
    public function index(){
        return view ('frontEnd/index')
        			->with('markets', Market::all())
        			->with('products', Product::all())
        			->with('categories', Category::all())
        			->with('shops', Shop::all());
    }

    public function devenirApacheur(){
        return view ('frontEnd.devenirApacheur')
                    ->with('markets', Market::all())
                    ->with('products', Product::all())
                    ->with('categories', Category::all())
                    ->with('shops', Shop::all());
    }

    public function detailProduct($id)
    {
        return View('frontEnd.productDetail')
                    ->with('markets', Market::all())
                    ->with('products', Product::all())
                    ->with('categories', Category::all())
                    ->with('shops', Shop::all())
                    ->with('product', Product::find($id));
    }

    public function shops(){
        return view ('frontEnd.shops')
                    ->with('markets', Market::all())
                    ->with('products', Product::all())
                    ->with('categories', Category::all())
                    ->with('shops', Shop::all());
    }

    public function shopDetail($id)
    {
        return View('frontEnd.shopDetail')
                    ->with('markets', Market::all())
                    ->with('products', Product::all())
                    ->with('categories', Category::all())
                    ->with('shops', Shop::all())
                    ->with('shop', Shop::find($id));
    }

    public function markets(){
        return view ('frontEnd.markets')
                    ->with('markets', Market::all())
                    ->with('products', Product::all())
                    ->with('categories', Category::all())
                    ->with('shops', Shop::all());
    }

    public function marketDetail($id)
    {
        return View('frontEnd.marketDetail')
                    ->with('markets', Market::all())
                    ->with('products', Product::all())
                    ->with('categories', Category::all())
                    ->with('shops', Shop::all())
                    ->with('market', Market::find($id));
    }
}
