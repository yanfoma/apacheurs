<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\User;
use App\Models\Sector;
use App\Models\Market;
use App\Models\Shop;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use DB;
use Storage;
use App\Http\Requests;
use App\Http\Controllers\controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function dashboard(){
        return view ('admin.dashboard');
    }

    public function sectors(){
        return view ('admin.sectors.index')
                    ->with('sectors', Sector::all());
    }

    public function markets(){
        return view ('admin.markets.index')
                    ->with('markets', Market::all())
                    ->with('sectors', Sector::all());
    }

    public function shops(){
        return view ('admin.shops.index')
                    ->with('markets', Market::all())
                    ->with('shops', Shop::all());
    }

    public function categories(){
        return view ('admin.categories.index')
                    ->with('categories', Category::all());
    }

    public function products(){
        return view ('admin.products.index')
                    ->with('categories', Category::all())
                    ->with('shops', Shop::all())
                    ->with('products', Product::all());
    }

    public function createUser(Request $request)
    {
        //
        $this->validate($request,[

            'identite'  => 'required',
            'email' => 'required|email',
            'telephone'  => 'required',
            'description' => 'required',
            'password' => 'required'
        ]);

        $user = User::create([
            'identity'      => $request->identite,
            'email'     => $request->email,
            'phone'     => $request->telephone,
            'description'     => $request->description,
            'password'  => Hash::make($request->Input('password')),
            'role'  => 'Owner',

        ]);

        return redirect()->route('login');
    }

    public function login(){
        return view ('admin.login');
    }

    public function log()
    {
        $data=array(
        'email'=>Input::get('email'),
        'password'=>Input::get('password'));

        if(Auth::attempt($data))
            {
                return Redirect()->route('dashboard');
            }
        else
            {
                return Redirect::to('login');
            }
    }

    public function createSector(Request $request)
    {
        //
        $this->validate($request,[

            'name'  => 'required',
            'description' => 'required',
            'city'  => 'required',
            'country' => 'required'
        ]);

        $sector = Sector::create([
            'name'      => $request->name,
            'description'     => $request->description,
            'city'     => $request->city,
            'country'     => $request->country,
            'created_by'  => Auth::user()->id

        ]);

        return Redirect::back();
    }

    public function createMarket(Request $request)
    {
        //
        $this->validate($request,[

            'name'  => 'required',
            'description' => 'required',
            'sector'  => 'required',
            'adresse' => 'required'
        ]);

        $market = Market::create([
            'name'      => $request->name,
            'description'     => $request->description,
            'sector_id'     => $request->sector,
            'adresse'     => $request->adresse,
            'created_by'  => Auth::user()->id

        ]);

        return Redirect::back();
    }

    public function createShop(Request $request)
    {
        //
        $this->validate($request,[

            'name'  => 'required',
            'description' => 'required',
            'email'  => 'required',
            'manager'  => 'required',
            'phone' => 'required'
        ]);

        $shop = Shop::create([
            'name'      => $request->name,
            'description'     => $request->description,
            'market_id'     => $request->market,
            'email'     => $request->email,
            'phone'     => $request->phone,
            'manager'     => $request->manager,
            'created_by'  => Auth::user()->id

        ]);

        return Redirect::back();
    }

    public function createCategory(Request $request)
    {
        //
        $this->validate($request,[

            'name'  => 'required',
            'description' => 'required'
        ]);

        $category = Category::create([
            'name'      => $request->name,
            'description'     => $request->description,
            'created_by'  => Auth::user()->id

        ]);

        return Redirect::back();
    }

    public function createProduct(Request $request)
    {
        //
        $this->validate($request,[

            'name'  => 'required',
            'description' => 'required',
            'price' => 'required',
            'quantity' => 'required',
            'product_category' => 'required',
            'shop' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $image  = $request->image;
        $image_new_name = time().$image->getClientOriginalName();
        $image->move('images/products/', $image_new_name);

        $product = Product::create([
            'name'      => $request->name,
            'description'     => $request->description,
            'price'     => $request->price,
            'quantity'     => $request->quantity,
            'shop_id'     => $request->shop,
            'product_category_id'     => $request->product_category,
            'image'     => $image_new_name,
            'created_by'  => Auth::user()->id

            ]);

        return Redirect::back();
    }
}
