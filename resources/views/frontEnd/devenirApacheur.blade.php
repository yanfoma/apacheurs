@include('layouts.frontEnd.header')
<body class="">
  <div class="se-pre-con"></div>
  <div class="main">
   
    <!-- HEADER START -->
    <header class="navbar navbar-custom container-full-sm" id="header">
      
      @include('layouts.frontEnd.headerMiddle')

      @include('layouts.frontEnd.headerBottom')

    </header>
    <!-- HEADER END -->  
    
    <section class="pt-70 pb-70">
    <div class="our_story gray-bg">
      <div class="container">
        <div class="row">
          <div class="col-md-6 ">
            <div class="story_detail_part">
              <div class="heading-part line-bottom mb-30">
                <h2 class="main_title  heading"><span>Qui est apacheur ?</span></h2>
              </div>
              <p style="text-align: justify;">Un apacheur est une personne qui dans le souci de vendre et rendre visible ses artciles ou produits, décide de créer une boutique en ligne et d'en assurer une gestion efficace et efficient de ceux-ci.</p>
              <p style="text-align: justify;">Si vous aussi vous désirez créer une boutique en ligne et la suivre, rien de plus simple que de devenir un apacheur en ligne en rejoingnant la communauté des apacheurs.</p>
              <b>Equipe Apacheurs</b> </div>
          </div>
          <div class="col-md-6 story_img_part">  
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="pt-50 pb-70">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="heading-part line-bottom mb-30">
            <h2 class="main_title  heading"><span>Devenir un apacheur</span></h2>
          </div>
        </div>
      </div>
      <div class="main-form">
        <form action="{{ route('createUser') }}" method="POST">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-md-3 mb-30">
              <input type="text" required placeholder="Votre identité" name="identite">
            </div>
            <div class="col-md-3 mb-30">
              <input type="text" required placeholder="Téléphone" name="telephone">
            </div>
            <div class="col-md-3 mb-30">
              <input type="Email" required placeholder="Email" name="email">
            </div>
            <div class="col-md-3 mb-30">
              <input type="password" required placeholder="Mot de passe" name="password">
            </div>
            <div class="col-12 mb-30">
              <textarea required placeholder="Décrivez-vous" rows="3" cols="30" name="description"></textarea>
            </div>
            <div class="col-12">
              <div class="align-center">
                <button type="submit" name="submit" class="btn btn-color">Rejoindre la communaté des apacheurs</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </section>
    <!-- CONTAINER END --> 

    @include('layouts.frontEnd.footer')
  </div>
  @include('layouts.frontEnd.scripts')

</body>
</html>
