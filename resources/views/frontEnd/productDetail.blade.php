@include('layouts.frontEnd.header')
<body class="">
  <div class="se-pre-con"></div>
  @include('layouts.frontEnd.popup')
  <div class="main">

    <!-- HEADER START -->
    <header class="navbar navbar-custom container-full-sm" id="header">

      @include('layouts.frontEnd.headerMiddle')

      @include('layouts.frontEnd.headerBottom')

      @include('layouts.frontEnd.popupLinks')
    </header>
    <!-- HEADER END --> 

    <!-- CONTAINER START -->
    <section class="pt-70">
      <div class="container">
        <div class="product-detail-view">
          <div class="">
            <div class="">
              <div class="row">
                <div class="col-md-5 mb-xs-30">
                  <div class="fotorama" data-nav="thumbs" data-allowfullscreen="native"> 
                    <a href="#"><img src="images/products/{{$product->image}}" alt=""></a> 
                  </div>
                </div>
                <div class="col-md-7">
                  <div class="product-detail-main">
                    <div class="product-item-details">
                      <h1 class="product-item-name">{{$product->name}}</h1>
                      <div class="rating-summary-block">
                        <div title="53%" class="rating-result"> <span style="width:53%"></span> </div>
                      </div>
                      <div class="price-box"> <span class="price">{{$product->price}} FCFA</span> </div>
                      <div class="product-info-stock-sku">
                        <div>
                          <label>Disponibilité : </label>
                          <span class="info-deta">En stock</span> 
                        </div>
                        <div>
                          <label>Boutique : </label>
                          <span class="info-deta">{{$product->shop_id}}</span> 
                        </div>
                      </div>
                      <p>{{$product->description}}</p>
                      <ul class="product-list">
                        <li><i class="fa fa-check"></i> 100% de satisfaction</li>
                        <li><i class="fa fa-check"></i> Bon produit de marque</li>
                        <li><i class="fa fa-check"></i> La qualité au rendez-vous </li>
                      </ul>
                 
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="ptb-70">
      <div class="container">
        <div class="product-listing">
          <div class="row">
            <div class="col-12">
              <div class="heading-part line-bottom mb-30">
                <h2 class="main_title heading"><span>Autres produits</span></h2>
              </div>
            </div>
          </div>
          <div class="pro_cat">
            <div class="row">
              <div class="owl-carousel pro-cat-slider">
                @foreach($products->take(8) as $product)
                <div class="item">
                  <div class="product-item">
                    <div class="main-label new-label"><span>{{$product->name}}</span></div>
                    <div class="main-label sale-label"><span>En vente</span></div>
                    <div class="product-image"> <a href="<?php echo url('productDetail');?><?php echo $product->id;?>"> <img src="images/products/{{$product->image}}" alt=""> </a>
                      <div class="product-detail-inner">
                        <div class="detail-inner-left align-center">
                          <ul>
                            <li class="pro-cart-icon">
                              <form>
                                <button title="Add to Cart"><i class="fa fa-shopping-basket"></i></button>
                              </form>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="product-item-details">
                      <div class="product-item-name"> <a href="<?php echo url('productDetail');?><?php echo $product->id;?>">{{$product->description}}</a> </div>
                      <div class="price-box"> <span class="price">{{$product->price}} FCFA</span> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </section>

    <!-- CONTAINER END --> 

    <!-- FOOTER START -->
    @include('layouts.frontEnd.footer')
    <div class="scroll-top">
      <div class="scrollup"></div>
    </div>
    <!-- FOOTER END --> 
  </div>
  @include('layouts.frontEnd.scripts')
</body>
</html>
