@include('layouts.frontEnd.header')
<body class="">
  <div class="se-pre-con"></div>
  @include('layouts.frontEnd.popup')
  <div class="main">

    <!-- HEADER START -->
    <header class="navbar navbar-custom container-full-sm" id="header">

      @include('layouts.frontEnd.headerMiddle')

      @include('layouts.frontEnd.headerBottom')

      @include('layouts.frontEnd.popupLinks')
    </header>
    <!-- HEADER END -->   

    <!-- CONTAIN START -->
    <section class="ptb-70">
      <div class="container">
        <div class="row">

          <div class="col-12">
            <div class="shorting shorting-style-2  mb-30">
              <div class="row">
                <div class="col-xl-6">
                  <div class="view">
                    <div class="list-types grid"> 
                      <a>
                        <div class="grid-icon list-types-icon"></div>
                      </a> 
                    </div>
                    <div class="list-types list active"> 
                      <a>
                        <div class="list-icon list-types-icon"></div>
                      </a> 
                    </div>
                  </div>
                  <div class="short-by"> <span>Sort By :</span>
                    <div class="select-item select-dropdown">
                      <fieldset>
                        <select  name="speed" id="sort-price" class="option-drop">
                          <option value="" selected="selected">Name (A to Z)</option>
                          <option value="">Name(Z - A)</option>
                        </select>
                      </fieldset>
                    </div>
                  </div>
                  <div class="slidebar-open btn-color btn closemenu">
                    <i class="fa fa-bars"></i> <span>Filter</span>
                  </div>
                </div>
                <div class="col-xl-6">
                  <div class="show-item float-left-sm"> <span>Show :</span>
                    <div class="select-item select-dropdown">
                      <fieldset>
                        <select  name="speed" id="show-item" class="option-drop">
                          <option value="" selected="selected">24</option>
                          <option value="">12</option>
                          <option value="">6</option>
                        </select>
                      </fieldset>
                    </div>
                    <span>Per Page</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="product-listing list-type">
              <div class="inner-listing">
                <div class="row">
                  @foreach($markets->take(120) as $market)
                  <div class="col-md-4 col-6 item-width mb-30">
                    <div class="product-item">
                      <div class="row">
                        <div class="img-col col-2">
                          <div class=""> 
                            <a href=""> 
                              <img src="images/marche.png" alt="" style="height: 170px"> 
                            </a>
                           
                          </div>
                        </div>
                        <div class="detail-col col-8">
                          <div class="product-item-details">
                            <div class="product-item-name"> 
                              <a href="">{{$market->name}}</a> 
                            </div>
                            <div class="product-des">
                              <p>{{$market->description}}</p>
                            </div>
                            <div class="product-des">
                              <p>{{$market->adresse}}</p>
                            </div>
                            <div class="product-des">
                              <p>{{$market->sector_id}}</p>
                            </div>
                          </div>
                        </div>
                      </div>    
                    </div>
                  </div>
                  @endforeach
                </div>
                
            </div>
          </div>

        </div>
      </div>
    </section>
    <!-- CONTAINER END --> 

    <!-- FOOTER START -->
    @include('layouts.frontEnd.footer')
    <div class="scroll-top">
      <div class="scrollup"></div>
    </div>
    <!-- FOOTER END -->   
  </div>
  @include('layouts.frontEnd.scripts')
</body>
</html>
