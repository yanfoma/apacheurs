@include('layouts.frontEnd.header')
<body class="homepage">
  <div class="se-pre-con"></div>
  @include('layouts.frontEnd.popup')
  <div class="main">
   
    <!-- HEADER START -->
    <header class="navbar navbar-custom container-full-sm" id="header">
      
      @include('layouts.frontEnd.headerMiddle')

      @include('layouts.frontEnd.headerBottom')

      @include('layouts.frontEnd.popupLinks')
    </header>
    <!-- HEADER END -->  
    
    <section class="main-wrap">
      <div class="container">
       
        @include('layouts.frontEnd.leftSidebar')
      </div>
    </section>
    <!-- CONTAINER END --> 

    @include('layouts.frontEnd.footer')
  </div>
  @include('layouts.frontEnd.scripts')

  <script>
  /* ------------ Newslater-popup JS Start ------------- */
  $(window).load(function() {
    $.magnificPopup.open({
      items: {src: '#newslater-popup'},type: 'inline'}, 0);
  });
    /* ------------ Newslater-popup JS End ------------- */
</script>

</body>
</html>
