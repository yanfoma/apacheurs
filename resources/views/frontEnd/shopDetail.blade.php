@include('layouts.frontEnd.header')
<body class="">
  <div class="se-pre-con"></div>
  @include('layouts.frontEnd.popup')
  <div class="main">

    <!-- HEADER START -->
    <header class="navbar navbar-custom container-full-sm" id="header">

      @include('layouts.frontEnd.headerMiddle')

      @include('layouts.frontEnd.headerBottom')

      @include('layouts.frontEnd.popupLinks')
    </header>
    <!-- HEADER END --> 

    <!-- CONTAINER START -->
    <section class="pt-70">
      <div class="container">
        <div class="product-detail-view">
          <div class="">
            <div class="">
              <div class="row">
                <div class="col-md-5 mb-xs-30">
                  <div class="fotorama" data-nav="thumbs" data-allowfullscreen="native"> 
                    <a href="#"><img src="images/boutiques.jpg" alt=""></a> 
                  </div>
                </div>
                <div class="col-md-7">
                  <div class="product-detail-main">
                    <div class="product-item-details">
                      <h1 class="product-item-name">{{$shop->name}}</h1>
                      <div class="rating-summary-block">
                        <div title="53%" class="rating-result"> <span style="width:53%"></span> </div>
                      </div>
                      <div class="price-box"> <span class="price"></span> </div>
                      
                      <p>{{$shop->description}}</p>
                      <ul class="product-list">
                        <li><i class="fa fa-check"></i> Email : {{$shop->email}}</li><br/>
                        <li><i class="fa fa-check"></i> Téléphone : {{$shop->phone}}</li><br/>
                        <li><i class="fa fa-check"></i> Manager : {{$shop->manager}} </li>
                      </ul>
                 
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="ptb-70">
      <div class="container">
        <div class="product-listing">
          <div class="row">
            <div class="col-12">
              <div class="heading-part line-bottom mb-30">
                <h2 class="main_title heading"><span>Autres boutiques</span></h2>
              </div>
            </div>
          </div>
          <div class="pro_cat">
            <div class="row">
              <div class="owl-carousel pro-cat-slider">
                @foreach($shops->take(8) as $shop)
                <div class="item">
                  <div class="product-item">
                    <div class="main-label new-label"><span></span></div>
                    <div class="main-label sale-label"><span></span></div>
                    <div class="product-image"> <a href="<?php echo url('shopDetail');?><?php echo $shop->id;?>"> <img src="images/boutiques.jpg" alt=""> </a>
                      <div class="product-detail-inner">
                        <div class="detail-inner-left align-center">
                          <ul>
                            <li class="pro-cart-icon">
                              <form>
                                <button title="Add to Cart"><i class="fa fa-shopping-basket"></i></button>
                              </form>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="product-item-details">
                      <div class="product-item-name"> <a href="<?php echo url('shopDetail');?><?php echo $shop->id;?>">{{$shop->name}}</a> </div>
                      <div class="price-box"> <span class="price">{{$shop->description}}</span> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </section>

    <!-- CONTAINER END --> 

    <!-- FOOTER START -->
    @include('layouts.frontEnd.footer')
    <div class="scroll-top">
      <div class="scrollup"></div>
    </div>
    <!-- FOOTER END --> 
  </div>
  @include('layouts.frontEnd.scripts')
</body>
</html>
