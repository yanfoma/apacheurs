 @include('layouts.header')
<body>
	@include('layouts.nav')
		
	@include('layouts.sidebar')
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Accueil</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Secteurs</h1>
			</div>
		</div><!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Nouveau secteur</div>
					<div class="panel-body">
						<div class="col-md-1">
							
							</div>
						<div class="col-md-5">
							<form role="form" method="POST" action="{{ route('createSector') }}">
							{{ csrf_field() }}
								<div class="form-group">
									<label>Intitulé</label>
									<input class="form-control" required="" autofocus="" name="name">
								</div>
																
								
								<div class="form-group">
									<label>Description</label>
									<input class="form-control" required="" name="description">
								</div>
								
							</div>
							<div class="col-md-5">
							
								<div class="form-group">
									<label>Ville</label>
									<input class="form-control" required="" name="city">
								</div>

								<div class="form-group">
									<label>Pays</label>
									<input class="form-control" required="" name="country">
								</div>
								
								
								<button type="submit" class="btn btn-primary">Enregistrer</button>
								<button type="reset" class="btn btn-default">Réinitialiser</button>
							</div>

							<div class="col-md-1">
							
							</div>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Liste des secteurs</div>
					<div class="panel-body">
						<table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
						    <thead>
						    <tr>
						        <th data-field="state" data-sortable="true">Intitulé</th>
						        <th data-field="id" data-sortable="true">Description</th>
						        <th data-field="name"  data-sortable="true">Ville</th>
						        <th data-field="price" data-sortable="true">Pays</th>
						    </tr>
						    </thead>
						    <tbody>
						    	 @foreach($sectors as $sector)
						    	 <td>{{$sector->name}}</td>
						    	 <td>{{$sector->description}}</td>
						    	 <td>{{$sector->city}}</td>
						    	 <td>{{$sector->country}}</td>
						    	 @endforeach
						    </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
								
	</div>	<!--/.main-->

	@include('layouts.scripts')
</body>

</html>
