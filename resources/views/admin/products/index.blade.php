 @include('layouts.header')
<body>
	@include('layouts.nav')
		
	@include('layouts.sidebar')
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Accueil</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Produits</h1>
			</div>
		</div><!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Nouveau produit</div>
					<div class="panel-body">
						<div class="col-md-1">
							
							</div>
						<div class="col-md-5">
							<form role="form" method="POST" action="{{ route('createProduct') }}" enctype="multipart/form-data">
							{{ csrf_field() }}
								<div class="form-group">
									<label>Intitulé</label>
									<input class="form-control" placeholder="" autofocus="" required="" name="name">
								</div>
																
								
								<div class="form-group">
									<label>Description</label>
									<input class="form-control" placeholder="" required="" name="description">
								</div>

								<div class="form-group">
									<label>Prix unitaire</label>
									<input class="form-control" placeholder="" type="number" required="" name="price" min="1">
								</div>

								<div class="form-group">
									<label>Quantité</label>
									<input class="form-control" placeholder="" type="number" required="" name="quantity" min="1">
								</div>
								
							</div>
							<div class="col-md-5">
							
								<div class="form-group">
									<label>Catégorie de produit</label>
									<select class="form-control" name="product_category" required="">
										<option></option>
										@foreach($categories as $category)
										<option value="{{$category->id}}">{{$category->name}}</option>
										@endforeach
									</select>
								</div>

								<div class="form-group">
									<label>Boutique</label>
									<select class="form-control" name="shop" required="">
										<option></option>
										@foreach($shops as $shop)
										<option value="{{$shop->id}}">{{$shop->name}}</option>
										@endforeach
									</select>
								</div>
								
								<div class="form-group">
									<label>Image</label>
									<input class="form-control" type="file" required="" name="image">
								</div>
								<br>
								<button type="submit" class="btn btn-primary">Enregistrer</button>
								<button type="reset" class="btn btn-default">Réinitialiser</button>
							</div>

							<div class="col-md-1">
							
							</div>
						</form>
					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Liste des produits</div>
					<div class="panel-body">
						<table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
						    <thead>
						    <tr>
						        <th data-field="state" data-sortable="true">Intitulé</th>
						        <th data-field="id" data-sortable="true">Description</th>
						        <th data-field="name"  data-sortable="true">Prix unitaire</th>
						        <th data-field="price" data-sortable="true">Quantité</th>
						        <th data-field="category"  data-sortable="true">Catégorie de produit</th>
						        <th data-field="shop" data-sortable="true">Boutique</th>
						    </tr>
						    </thead>
						    <tbody>
						    	 @foreach($products as $product)
						    	 <td>{{$product->name}}</td>
						    	 <td>{{$product->description}}</td>
						    	 <td>{{$product->price}}</td>
						    	 <td>{{$product->quantity}}</td>
						    	 <td>{{$product->product_category_id}}</td>
						    	 <td>{{$product->shop_id}}</td>
						    	 @endforeach
						    </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
								
	</div>	<!--/.main-->

	@include('layouts.scripts')
</body>

</html>
