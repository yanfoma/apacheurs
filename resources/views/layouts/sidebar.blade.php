<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li class="{{\Request::is('dashboard') ?'active':''}}"><a href="{{route('dashboard')}}"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Tableau de bord</a></li>
			<li class="{{\Request::is('sectors') ?'active':''}}"><a href="{{route('sectors')}}"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg> Secteurs</a></li>
			<li class="{{\Request::is('markets') ?'active':''}}"><a href="{{route('markets')}}"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg> Marchés</a></li>
			<li class="{{\Request::is('shops') ?'active':''}}"><a href="{{route('shops')}}"><svg class="glyph stroked table"><use xlink:href="#stroked-table"></use></svg> Boutiques</a></li>
			<li class="{{\Request::is('categories') ?'active':''}}"><a href="{{route('categories')}}"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg> Categories de produits</a></li>
			<li class="{{\Request::is('products') ?'active':''}}"><a href="{{route('products')}}"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Produits</a></li>
			<li class="{{\Request::is('pubs') ?'active':''}}"><a href="{{route('pubs')}}"><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg> Publications</a></li>

			<li role="presentation" class="divider"></li>
			<li><a href="{{route('index')}}"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Apacheurs</a></li>
		</ul>

	</div>