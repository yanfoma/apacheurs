<!DOCTYPE html>
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<head>
<!-- Basic Page Needs
  ================================================== -->
<meta charset="utf-8">
<title>Apacheurs - Website</title>
<!-- SEO Meta
  ================================================== -->
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="distribution" content="global">
<meta name="revisit-after" content="2 Days">
<meta name="robots" content="ALL">
<meta name="rating" content="8 YEARS">
<meta name="Language" content="en-us"> 
<meta name="GOOGLEBOT" content="NOARCHIVE">
<!-- Mobile Specific Metas
  ================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- CSS
  ================================================== -->
<link rel="stylesheet" type="text/css" href="css/frontEnd/font-awesome.min.css"/>
<link rel="stylesheet" type="text/css" href="css/frontEnd/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="css/frontEnd/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="css/frontEnd/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="css/frontEnd/fotorama.css">
<link rel="stylesheet" type="text/css" href="css/frontEnd/magnific-popup.css">
<link rel="stylesheet" type="text/css" href="css/frontEnd/custom.css">
<link rel="stylesheet" type="text/css" href="css/frontEnd/responsive.css">
<link rel="shortcut icon" href="images/favicon.png">
<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
</head>