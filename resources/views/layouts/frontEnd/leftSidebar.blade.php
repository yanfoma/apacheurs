<div class="row">
  <div class="col-xl-2 col-lg-3 col-lgmd-20per mt-30 left-side float-none-sm ">
    <div class="sidebar-block open1">
      <div class="sidebar-box sidebar-item mb-30"> <span class="opener plus"></span>
        <div class="sidebar-title">
          <h3>Marchés</h3> <span></span>
        </div>
        <div class="sidebar-contant">
          <div class="row">
            <div id="sidebar-product" class="owl-carousel">
             
              <div class="item">
                <ul>
                 @foreach($markets->take(3) as $market)
                  <li>
                    <div class="pro-media"> <a><img alt="T-shirt" src="images/marche.png"></a> </div>
                    <div class="pro-detail-info"> <a>{{$market->name}}</a>
                      <div class="rating-summary-block">
                        <div class="rating-result" title="53%"> <span style="width:53%"></span> </div>
                      </div>
                      <div class="price-box"> <span class="price">{{$market->adresse}}</span> </div>
                    </div>
                  </li>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="sidebar-box left-banner sub-banner mb-30 d-none d-lg-block"> 
        <a href="#"> 
          <img src="images/left-banner.jpg" alt=""> 
        </a> 
      </div>

      <!--Blog Block Start -->
      <div class="sidebar-box mb-30"><span class="opener plus"></span>
        <div class="">
          <div class="sidebar-title">
            <h3>Temoignages</h3> <span></span>
          </div>
          <div class="sidebar-contant">
            <div id="blog"  class="owl-carousel">
              <div class="item">
                <div class="blog-item">
                  <div class="">
                    <div class="blog-media"> 
                      <img src="images/temoin.jpg" alt="">
                      <div class="blog-effect"></div>
                      <div class="effect"></div>  
                    </div>
                  </div>
                  <div class="">
                    <div class="blog-detail mt-30"> 
                      <span>Ouédraogo Faishal</span>
                      <div class="post-info">
                        <p> La plateforme de mise en relation entre clients et vendeurs.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="blog-item">
                  <div class="">
                    <div class="blog-media"> 
                      <img src="images/temoin.jpg" alt="">
                      <div class="blog-effect"></div>
                      <div class="effect"></div>  
                    </div>
                  </div>
                  <div class="">
                    <div class="blog-detail mt-30">
                      <span>Kaboré Romuald</span>
                      <div class="post-info">
                        <p> J'adore franchement cette vitrine</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--Blog Block End -->

      <!--  Site Services Features Block Start  -->
      <div class="ser-feature-block ">
        <div class="">
          <div class="">
            <ul class="">
              <li class="service-box">
                <div class="feature-box ">
                  <div class="feature-icon feature1"></div>
                  <div class="feature-detail">
                    <div class="ser-title">Simple</div>
                    <div class="ser-subtitle">d'utilisation</div>
                  </div>
                </div>
              </li>
              <li class="service-box">
                <div class="feature-box">
                  <div class="feature-icon feature2"></div>
                  <div class="feature-detail">
                    <div class="ser-title">Service aide</div>
                    <div class="ser-subtitle">pour vous</div>
                  </div>
                </div>
              </li>
              
              <li class="service-box">
                <div class="feature-box ">
                  <div class="feature-icon feature4"></div>
                  <div class="feature-detail">
                    <div class="ser-title">Economies</div>
                    <div class="ser-subtitle">d'argent</div>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <!--  Site Services Features Block End  -->
    </div>
  </div>
  <div class="col-xl-10 col-lg-9 col-lgmd-80per mt-30 right-side float-none-sm float-right-imp">

   @include('layouts.frontEnd.slideShow')

   <!-- CONTAIN START -->
   <!--  Featured Products Slider Block Start  -->
   @include('layouts.frontEnd.recentProducts')
   <!--  Featured Products Slider Block End  -->

   <!-- SUB-BANNER START -->
   @include('layouts.frontEnd.pubs')
   <!-- SUB-BANNER END -->

   <!--  Special products Products Slider Block Start  -->
   @include('layouts.frontEnd.recentShops')
   <!--  Special products Products Slider Block End  -->

 </div>
</div>