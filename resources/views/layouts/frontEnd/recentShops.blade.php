<section class="mt-60">
  <div class="cat-block">
    <div class="">
      <div class="">
        <div class="">
          <div class="pro_cat mb-xs-30">
            <div class="row m-0">
              <div class="col-12 p-0">
                <div class="heading-part line-bottom mb-30">
                  <h2 class="main_title heading"><span>Les boutiques disponibles</span></h2>
                </div>
              </div>
            </div>
            <div class="row mlr_-20">
              <div class="owl-carousel best-seller-pro">
                @foreach($shops->take(12) as $shop)
                <div class="item plr-20">
                  <div class="cat-box">
                    <div class="cat-box-inner">
                      <ul>
                        <li>
                          <div class="pro-media"> 
                            <a href=""><img src="images/boutiques.jpg" alt=""></a> 
                          </div>
                          <div class="pro-detail-info">
                            <div class="rating-summary-block">
                              <div title="53%" class="rating-result">
                                <span style="width:53%"></span>
                              </div>
                            </div>
                            <a href="<?php echo url('shopDetail');?><?php echo $shop->id;?>">{{$shop->name}}</a><br/>
                            <div class="price-box">
                              <span class="price">{{$shop->email}}</span>
                            </div>
                            <div class="cart-link">
                                <button><span></span>{{$shop->manager}}</button>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>