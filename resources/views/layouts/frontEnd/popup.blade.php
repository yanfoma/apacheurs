<div id="newslater-popup" class="mfp-hide white-popup-block open align-center">
  <div class="nl-popup-main">
    <div class="nl-popup-inner">
      <div class="newsletter-inner">
        <div class="row">
          <div class="col-md-4"></div>
          <div class="col-md-8">
            <h2 class="main_title">Rejoignez nous maintenant</h2>
            <span>Pour etre notifier de l'ajout de nouveaux articles</span>
            <p>Souscrivez dès à présent à la newsletter pour recevoir les différentes offres récentes !</p>
            <form>
              <input type="email" placeholder="Entrez votre email">
              <button class="btn-black" title="Subscribe">Souscrire</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>