<div class="header-bottom"> 
  <div class="container">
    <div class="header-line"> 
      <div class="row position-r">
        <div class="col-xl-2 col-lg-3 bottom-part col-lgmd-20per position-initial">
          <div class="sidebar-menu-dropdown home">
            <a class="btn-sidebar-menu-dropdown"><i class="fa fa-bars"></i> Categories </a>
            <div id="cat" class="cat-dropdown">
              <div class="sidebar-contant">
                <div id="menu" class="navbar-collapse collapse">

                  <ul class="nav navbar-nav ">
                    <li class="level sub-megamenu">
                      @foreach($categories->take(10) as $category)
                      <span class="opener plus"></span>
                      <a href="" class="page-scroll"><i class="fa fa-female"></i>{{$category->name}}</a>
                      <div class="megamenu mobile-sub-menu" style="width:430px;">
                        <div class="megamenu-inner-top">
                          <ul class="sub-menu-level1">
                            <li class="level2">
                              <span>{{$category->name}}</span>
                              </li>
                            </ul>
                          </div>
                        </div>
                        @endforeach
                      </li>

                    </ul>

                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-6 col-lg-6 bottom-part col-lgmd-60per">
            <div class="header-right-part">
              <div class="category-dropdown select-dropdown">
                <fieldset>
                  <select id="search-category" class="option-drop" name="search-category">
                    <option>Toutes les catégories</option>
                    @foreach($categories as $category)
                    <option value="{{$category->id}}">■ {{$category->name}}</option>
                    @endforeach
                  </select>
                </fieldset>
              </div>
              <div class="main-search">
                <div class="header_search_toggle desktop-view">
                  <form>
                    <div class="search-box">
                      <input class="input-text" type="text" placeholder="Rechercher ici...">
                      <button class="search-btn"></button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-4 col-lg-3 bottom-part col-lgmd-20per">
            <div class="right-side float-left-xs header-right-link">
              <ul>
                <li class="support-icon">
                  <a href="#">
                    <span class="support-icon-main"></span>
                    <div class="my-cart">Customer<br>support 24/7 </div>
                  </a>
                </li>
                <li class="cart-icon"></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>