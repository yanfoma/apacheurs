<div class="popup-links ">
      <div class="popup-links-inner">
        <ul>
          <li class="categories">
            <a class="popup-with-form" href="#categories_popup"><span class="icon"><i class="fa fa-bars"></i></span><span class="icon-text">Produits</span></a>
          </li>
          
          <li class="search">
            <a class="popup-with-form" href="#search_popup"><span class="icon"><i class="fa fa-search"></i></span><span class="icon-text">Recherche</span></a>
          </li>
          <li class="scroll scrollup">
            <a href="#"><span class="icon"><i class="fa fa-chevron-up"></i></span><span class="icon-text">Scroll-top</span></a>
          </li>
        </ul>
      </div>
      <div id="popup_containt">
        <div id="categories_popup" class="white-popup-block mfp-hide popup-position">
          <div class="popup-title">
            <h2 class="main_title heading"><span>Produits</span></h2>
          </div>
          <div class="popup-detail">
            <ul class="cate-inner">
              @foreach($products->take(18) as $product)
              <li class="level">
                <a href="<?php echo url('productDetail');?><?php echo $product->id;?>" class="page-scroll"><i class="fa fa-camera-retro"></i>{{$product->name}} à  {{$product->price}} FCFA</a>
              </li>
              @endforeach
            </ul>
          </div>  
        </div>
        
        <div id="search_popup" class="white-popup-block mfp-hide popup-position">
          <div class="popup-title">
            <h2 class="main_title heading"><span>Recherche</span></h2>
          </div>            
          <div class="popup-detail">
            <div class="main-search">
              <div class="header_search_toggle desktop-view">
                <form>
                  <div class="search-box">
                    <input class="input-text" type="text" placeholder="Rechercher ici...">
                    <button class="search-btn"></button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>