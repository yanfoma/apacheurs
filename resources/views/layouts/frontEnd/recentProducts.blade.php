<div class="featured-product mt-60 mt-sm-30">
  <div class="product-listing">
    <div class="row">
      <div class="col-12">
        <div class="heading-part line-bottom mb-30">
          <div id="tabs" class="category-bar">
            <ul class="tab-stap">
              <li><a class="tab-step1 selected" title="step1">Récents produits</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="product-listing grid-type">
            <div class="inner-listing">
              <div class="row">
                @foreach($products->take(8) as $product)
                <div class="col-md-4 col-6 item-width mb-30" style="padding-right: 20px">
                  <div class="product-item">
                    <div class="row">
                      <div class="img-col col-12">
                        <div class="product-image"> 
                          <a href=""> 
                            <img src="images/products/{{$product->image}}" alt="" style="width: 100%; height: 300px"> 
                          </a>
                          <div class="product-detail-inner">
                            <div class="detail-inner-left align-center">
                              <ul>
                                <li class="pro-cart-icon">
                                  <form>
                                    <button title="Add to Cart"><i class="fa fa-shopping-basket"></i></button>
                                  </form>
                                </li>
                                <li class="pro-quick-view-icon"><a class="popup-with-product" href="#product_popup" title="quick-view"><i class="fa fa-eye"></i></a></li>
                              </ul>
                            </div>
                          </div>
                        </div>
                        <div class="product-item-details">
                          <div class="product-item-name"> <a href="<?php echo url('productDetail');?><?php echo $product->id;?>">{{$product->name}}</a> </div>
                          <div class="price-box"> <span class="price">{{$product->price}} FCFA</span> </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  {{--<div id="product_popup" class="quick-view-popup white-popup-block mfp-hide popup-position ">--}}
    {{--<div class="popup-detail">--}}
      {{--<div class="container">--}}
        {{--<div class="row">--}}
          {{--<div class="col-lg-12">--}}
            {{--<div class="row">--}}
              {{--<div class="col-lg-5 col-md-5 mb-xs-30">--}}
                {{--<div class="fotorama" data-nav="thumbs" data-allowfullscreen="native"> --}}
                  {{--<a href="#"><img src="images/products/{{$product->image}}" alt=""></a> --}}
                {{--</div>--}}
              {{--</div>--}}
              {{--<div class="col-lg-7 col-md-7">--}}
                {{--<div class="row">--}}
                  {{--<div class="col-12">--}}
                    {{--<div class="product-detail-main">--}}
                      {{--<div class="product-item-details">--}}
                        {{--<h1 class="product-item-name">{{$product->name}}</h1>--}}
                        {{--<div class="rating-summary-block">--}}
                          {{--<div title="53%" class="rating-result"> <span style="width:53%"></span> </div>--}}
                        {{--</div>--}}
                        {{--<div class="price-box"> <span class="price">{{$product->price}} FCFA</span> </div>--}}
                        {{--<div class="product-info-stock-sku">--}}
                          {{--<div>--}}
                            {{--<label>Disponibilité : </label>--}}
                            {{--<span class="info-deta">En stock</span> --}}
                          {{--</div>--}}
                          {{--<div>--}}
                            {{--<label>Boutique : </label>--}}
                            {{--<span class="info-deta">{{$product->shop_id}}</span> --}}
                          {{--</div>--}}
                        {{--</div>--}}
                        {{--<p>{{$product->description}}</p>--}}
                        {{--<ul class="product-list">--}}
                          {{--<li><i class="fa fa-check"></i> 100% de satisfaction</li>--}}
                          {{--<li><i class="fa fa-check"></i> Bon produit de marque</li>--}}
                          {{--<li><i class="fa fa-check"></i> La qualité au rendez-vous </li>--}}
                        {{--</ul>--}}
                      {{--</div>--}}
                    {{--</div>--}}
                  {{--</div>--}}
                {{--</div>--}}
              {{--</div>--}}
            {{--</div>--}}
          {{--</div>--}}
        {{--</div>--}}
      {{--</div>--}}
    {{--</div>  --}}
  {{--</div>--}}
</div>
</div>