<div class="header-middle">
        <div class="container">
          <div class="row">
            <div class="col-xl-2 col-lg-3 col-lgmd-20per">
              <div class="header-middle-left">
                <div class="navbar-header float-none-sm">
                  <a class="navbar-brand page-scroll" href="">
                    <img alt="" src="images/logo.png">
                  </a> 
                </div>
              </div>
            </div>
            <div class="col-xl-10 col-lg-9 col-12 col-lgmd-80per">
              <div class="bottom-inner right-side float-none-sm">
                <div class="position-r">          
                  <div class="nav_sec position-r">
                    <div class="mobilemenu-title mobilemenu">
                      <span>Menu</span>
                      <i class="fa fa-bars pull-right"></i>
                    </div>
                    <div class="mobilemenu-content">
                      <ul class="nav navbar-nav" id="menu-main">
                        <li class="{{\Request::is('/') ?'active':''}}">
                          <a href="{{Route('index')}}"><span>Accueil</span></a>
                        </li>
                        <li class="level dropdown {{\Request::is('markets') ?'active':''}}">
                          <span class="opener plus"></span>
                          <a href="{{Route('markets')}}"><span>Marchés</span></a>
                          <div class="megamenu mobile-sub-menu">
                            <div class="megamenu-inner-top">
                              <ul class="sub-menu-level1">
                                <li class="level2">
                                  <ul class="sub-menu-level2 ">
                                    @foreach($markets->take(10) as $market)
                                    <li class="level3"><span>■</span>{{$market->name}}</li>
                                    @endforeach
                                  </ul>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </li>
                        <li class="level dropdown {{\Request::is('shops') ?'active':''}}">
                          <span class="opener plus"></span>
                          <a href="{{Route('shops')}}"><span>Boutiques</span></a>
                          <div class="megamenu mobile-sub-menu">
                            <div class="megamenu-inner-top">
                              <ul class="sub-menu-level1">
                                <li class="level2">
                                  <ul class="sub-menu-level2 ">
                                    @foreach($shops->take(10) as $shop)
                                    <li class="level3"><a href="<?php echo url('shopDetail');?><?php echo $shop->id;?>"><span>■</span>{{$shop->name}}</a></li>
                                    @endforeach
                                  </ul>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </li>
                        <li class="{{\Request::is('devenirApacheur') ?'active':''}}">
                          <a href="{{Route('devenirApacheur')}}"><span>Devenir apacheur</span></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>