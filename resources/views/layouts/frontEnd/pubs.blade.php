<div class="sub-banner-block mt-60 ">
  <div class="">
    <div class=" center-sm">
      <div class="row">
        <div class="col-xl-7 col-md-12">
          <div class="sub-banner sub-banner1" >
            <img alt="" src="images/sub-banner1.jpg">
            <div class="sub-banner-detail">
              <div class="sub-banner-title sub-banner-title-color">Jewellery for Women</div>
              <div class="sub-banner-subtitle">wide range of Jewellery upto 50% off</div>
              <a class="btn btn-color" href="">Shop Now!</a>
            </div>
          </div>
        </div>
        <div class="col-xl-5 col-md-6 d-none d-xl-block">
          <div class="sub-banner sub-banner2">
            <img alt="" src="images/sub-banner2.jpg">
            <div class="sub-banner-detail">
              <div class="video-block">
                <a class="popup-youtube" href="https://player.vimeo.com/video/19228643">
                  <img alt="" src="images/you-tube-icon.png" draggable="false">
                </a>
              </div>
              <div class="sub-banner-subtitle">Must See Video </div>
              <div class="sub-banner-title sub-banner-title-color">Hurry ! Don’t miss it</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>