<div class="footer">
    <div class="container">
      <div class="footer-inner">
        <div class="footer-middle">
          <div class="row">
            <div class="col-xl-4 f-col">
              <div class="footer-static-block"> <span class="opener plus"></span>
                <h3 class="title">adresse<span></span></h3>
                <ul class="footer-block-contant address-footer">
                  
                  <li class="item"> <i class="fa fa-envelope"> </i>
                    <p> <a href="#">infoservices@apacheurs.com </a> </p>
                  </li>
                  <li class="item"> <i class="fa fa-phone"> </i>
                    <p>(+237) 673 71 31 15</p>
                  </li>
                </ul>
              </div>
            </div>
            
          </div>
        </div>
        <hr>
        <div class="footer-bottom ">
          <div class="row mtb-30">
            <div class="col-lg-6 ">
              <div class="footer_social mb-sm-30 center-sm">
                <ul class="social-icon">
                  <li><div class="title">Suivez nous sur :</div></li>
                  <li><a title="Facebook" class="facebook"><i class="fa fa-facebook"> </i></a></li>
                  <li><a title="Twitter" class="twitter"><i class="fa fa-twitter"> </i></a></li>
                  <li><a title="Linkedin" class="linkedin"><i class="fa fa-linkedin"> </i></a></li>
                </ul>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
    <div class="copy-right-bg">
      <div class="container">
        <div class="row  align-center">
          
          <div class="col-12">
            <div class="">
              <div class="copy-right ">© 2018 All Rights Reserved. Design By  <a href="www.yanfoma.tech" target="_blank">Yanfoma</a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="scroll-top">
    <div class="scrollup"></div>
  </div>